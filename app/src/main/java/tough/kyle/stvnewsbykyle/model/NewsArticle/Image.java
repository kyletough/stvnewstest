package tough.kyle.stvnewsbykyle.model.NewsArticle;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Kyle on 18/01/2016.
 */
public class Image implements Serializable {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("caption")
        @Expose
        private String caption;
        @SerializedName("description")
        @Expose
        private String description;

        /**
         *
         * @return
         * The id
         */
        public String getId() {
            return id;
        }

        /**
         *
         * @param id
         * The id
         */
        public void setId(String id) {
            this.id = id;
        }

        /**
         *
         * @return
         * The caption
         */
        public String getCaption() {
            return caption;
        }

        /**
         *
         * @param caption
         * The caption
         */
        public void setCaption(String caption) {
            this.caption = caption;
        }

        /**
         *
         * @return
         * The description
         */
        public String getDescription() {
            return description;
        }

        /**
         *
         * @param description
         * The description
         */
        public void setDescription(String description) {
            this.description = description;
        }



}