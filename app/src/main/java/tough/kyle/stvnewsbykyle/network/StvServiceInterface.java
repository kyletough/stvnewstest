package tough.kyle.stvnewsbykyle.network;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import tough.kyle.stvnewsbykyle.model.ArticleImage;
import tough.kyle.stvnewsbykyle.model.NewsArticle.NewsArticle;

/**
 * Retrofit service interface to hold the endpoints for the base url
 * Created by Kyle on 18/01/2016.
 */
public interface StvServiceInterface {

    //http://api.stv.tv/articles/?count=50&navigationLevelId=1218&orderBy=ranking+DESC%2C+createdAt+DESC&full=1&count=20
    @GET("articles/?count=50&navigationLevelId=1218&orderBy=ranking+DESC%2C+createdAt+DESC&full=1&count=20")
    Call<List<NewsArticle>> getNewsArticles();

    //http://api.stv.tv/images/316051/rendition/?width=640&height=360
    @GET("images/{imageId}/rendition/")
    Call<ArticleImage> getImageWithWidthAndHeight(@Path("imageId") String imageId,
                                                  @Query(value="width") String width,
                                                  @Query(value="height") String height);


}
