package tough.kyle.stvnewsbykyle.network;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by Kyle on 18/01/2016.
 */
public class RetrofitClient {

    private static final String TAG = RetrofitClient.class.getSimpleName();

    private static final String BASE_URL = "http://api.stv.tv/";

    private static StvServiceInterface sStvServiceInterface;
    private static Context mContext;

    /**
     * Retrofit client
     * @param context
     * @return
     */
    public static StvServiceInterface getStvServiceClient(Context context) {
        if (sStvServiceInterface == null) {
            mContext = context;

            // Create GSON builder, incase of custom deseriliser its easy to add them here
            Gson gson = new GsonBuilder().create();

            // Create Executor - to allow parallel downloading using threadpool
            Executor executor = Executors.newCachedThreadPool();

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .callbackExecutor(executor)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            sStvServiceInterface = client.create(StvServiceInterface.class);
        }
        return sStvServiceInterface;
    }
}
