package tough.kyle.stvnewsbykyle.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import tough.kyle.stvnewsbykyle.R;
import tough.kyle.stvnewsbykyle.model.NewsArticle.NewsArticle;

/**
 * Created by Kyle on 18/01/2016.
 */
public class NewsArticleAdapter extends BaseAdapter {

    private static final String TAG = NewsArticleAdapter.class.getSimpleName();

    List<NewsArticle> newsList = new ArrayList<>();
    LayoutInflater inflater;
    Context context;


    public NewsArticleAdapter(Context context, List<NewsArticle> articleList) {
        this.newsList = articleList;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return newsList.size();
    }

    @Override
    public NewsArticle getItem(int position) {
        return newsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder mViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_news_article_row, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        NewsArticle currentNewsArticle = getItem(position);

        // Set title
        mViewHolder.tvTitle.setText(currentNewsArticle.getTitle());
        //Sub heading
        mViewHolder.tvSubHeadline.setText(currentNewsArticle.getSubHeadline());

        // CHeck if images are null then loop through and add images
        // Originally I looped through all images but only deal with one
        // so now take the first image and display it, should hopefully speed the population of images
        if (currentNewsArticle.getThumbImages() != null) {
            // Check if it contains a value at 0
            if (currentNewsArticle.getThumbImages().size() > 0) {
                Picasso.with(context).load(currentNewsArticle.getThumbImages().get(0).getUrl()).into(mViewHolder.ivMainImage);
                mViewHolder.mProgressBar.setVisibility(View.VISIBLE);
                mViewHolder.ivMainImage.setVisibility(View.VISIBLE);
                final ProgressBar progressView = mViewHolder.mProgressBar;
                Picasso.with(context)
                        .load(currentNewsArticle.getThumbImages().get(0).getUrl())
                        .fit()
                        .into(mViewHolder.ivMainImage, new Callback() {
                            @Override
                            public void onSuccess() {
                                progressView.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                // TODO Auto-generated method stub

                            }
                        });
            }else {
                Log.i(TAG, "getView: No url in 0");
            }

            }else{
                Log.i(TAG, "getView: Thumbnail is null");
            }

        return convertView;
    }

    private class MyViewHolder {
        TextView tvTitle, tvSubHeadline;
        ImageView ivMainImage;
        ProgressBar mProgressBar;

        public MyViewHolder(View item) {
            tvTitle = (TextView) item.findViewById(R.id.tvArticleTitle);
            tvSubHeadline = (TextView) item.findViewById(R.id.tvArticleSubHeadline);
            ivMainImage = (ImageView) item.findViewById(R.id.ivArticleMainImage);
            mProgressBar = (ProgressBar) item.findViewById(R.id.progressBarListImage);
        }
    }
}