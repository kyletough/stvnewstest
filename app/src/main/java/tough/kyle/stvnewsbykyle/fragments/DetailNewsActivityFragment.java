package tough.kyle.stvnewsbykyle.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import tough.kyle.stvnewsbykyle.R;
import tough.kyle.stvnewsbykyle.model.ArticleImage;
import tough.kyle.stvnewsbykyle.model.NewsArticle.Image;
import tough.kyle.stvnewsbykyle.network.RetrofitClient;
import tough.kyle.stvnewsbykyle.network.StvServiceInterface;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailNewsActivityFragment extends Fragment implements Callback<ArticleImage> {

    private static final String TAG = DetailNewsActivityFragment.class.getSimpleName();
    private static final String ARG_MAIN_TO_DETAIL = "article";
    private static final String ARG_MAIN_TO_DETAIL_IMAGE = "images";
    private static final String ARG_MAIN_TO_DETAIL_TITLE = "title" ;
    private static final String ARG_MAIN_TO_DETAIL_IMAGE_THUMB = "thumbs";

    private static final String WIDTH = "1024";
    private static final String HEIGHT = "768";

    // Butterknife views
    @Bind(R.id.ivDetailMainImage)
    ImageView ivDetailMainImage;
    @Bind(R.id.webView)
    WebView mWebview;



    private String article;
    private String title;
    private List<ArticleImage> mArticleImageList = new ArrayList<>();
    private List<Image> mImageList = new ArrayList<>();

    public DetailNewsActivityFragment() {
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Take passed variables and assign them to values
        Bundle extras =getActivity().getIntent().getExtras();
        if (extras != null) {
            title = extras.getString(ARG_MAIN_TO_DETAIL_TITLE);
            article = extras.getString(ARG_MAIN_TO_DETAIL);
            mArticleImageList = (List<ArticleImage>) extras.getSerializable(ARG_MAIN_TO_DETAIL_IMAGE_THUMB);
            mImageList = (List<Image>) extras.getSerializable(ARG_MAIN_TO_DETAIL_IMAGE);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_detail_news, container, false);
        ButterKnife.bind(this, rootView);
        setRetainInstance(true);

        // Get a larger version of image for detail screen
        // If unsuccessful use the thumbnail image already downloaded
        if (mImageList.size() != 0) {
            // prepare call in Retrofit 2.0
            StvServiceInterface stvServiceClient = RetrofitClient.getStvServiceClient(getContext());
            Call<ArticleImage> articleImageCall = stvServiceClient.getImageWithWidthAndHeight(mImageList.get(0).getId(), WIDTH, HEIGHT);

            articleImageCall.enqueue(this);
        }

        // Change the title in the actionbar
        getActivity().setTitle(title);

        // Load the content into webview
        mWebview.loadData(article, "text/html; charset=UTF-8", null);


        return rootView;
    }

    /**
     * Retrofit Successful respones
     * @param response
     */
    @Override
    public void onResponse(Response<ArticleImage> response) {
        final ArticleImage articleImage = response.body();
        Log.i(TAG, "onResponse: SUcess");
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Picasso.with(getActivity()).load(articleImage.getUrl()).into(ivDetailMainImage);

            }
        });

    }

    /**
     * Retrofit failure and has throwable to find out reasons for failure
     * @param t
     */
    @Override
    public void onFailure(Throwable t) {
// Load the passed image
        Log.i(TAG, "onFailure: failure", t.getCause());
        Picasso.with(getActivity()).load(mArticleImageList.get(0).getUrl()).into(ivDetailMainImage);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(ARG_MAIN_TO_DETAIL_TITLE, title);
        outState.putString(ARG_MAIN_TO_DETAIL, article);
        outState.putSerializable(ARG_MAIN_TO_DETAIL_IMAGE_THUMB, (Serializable) mArticleImageList);
        outState.putSerializable(ARG_MAIN_TO_DETAIL_IMAGE, (Serializable) mImageList);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            title = savedInstanceState.getString(ARG_MAIN_TO_DETAIL_TITLE);
            article = savedInstanceState.getString(ARG_MAIN_TO_DETAIL);
            mArticleImageList = (List<ArticleImage>) savedInstanceState.getSerializable(ARG_MAIN_TO_DETAIL_IMAGE_THUMB);
            mImageList = (List<Image>) savedInstanceState.getSerializable(ARG_MAIN_TO_DETAIL_IMAGE);
        }
    }
}
