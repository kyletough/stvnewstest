package tough.kyle.stvnewsbykyle.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import tough.kyle.stvnewsbykyle.R;
import tough.kyle.stvnewsbykyle.activities.DetailNewsActivity;
import tough.kyle.stvnewsbykyle.adapters.NewsArticleAdapter;
import tough.kyle.stvnewsbykyle.model.ArticleImage;
import tough.kyle.stvnewsbykyle.model.NewsArticle.Image;
import tough.kyle.stvnewsbykyle.model.NewsArticle.NewsArticle;
import tough.kyle.stvnewsbykyle.network.RetrofitClient;
import tough.kyle.stvnewsbykyle.network.StvServiceInterface;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements Callback<List<NewsArticle>> {

    private static final String TAG = MainActivityFragment.class.getSimpleName();
    private static final String ARG_MAIN_TO_DETAIL = "article";
    private static final String ARG_MAIN_TO_DETAIL_IMAGE = "images";
    private static final String ARG_MAIN_TO_DETAIL_TITLE = "title";
    private static final String ARG_MAIN_TO_DETAIL_IMAGE_THUMB = "thumbs";

    // Butterknife views
    @Bind(R.id.rlMainFragment)
    RelativeLayout mRelativeLayout;
    @Bind(R.id.progressBarMainActivity)
    ProgressBar spinner;
    @Bind(R.id.lvNewsArticles)
    ListView mListView;

    // Variables to be used
    List<NewsArticle> mNewsArticleList = new ArrayList<>();
    ArticleImage mArticleImage;
    StvServiceInterface stvServiceClient;
    NewsArticleAdapter adapter;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, rootView);
        setRetainInstance(true);

        // Check if savedInstanceState is null
        // will load in saved values if it isnt
        // or recall the webserver if it is
        if (savedInstanceState != null) {

            setNewsArticleList((List<NewsArticle>) savedInstanceState.getSerializable("newsArticle"));

            adapter = new NewsArticleAdapter(getActivity(), getNewsArticleList());
            mListView.setAdapter(adapter);
            spinner.setVisibility(View.GONE);
        } else {
            // prepare call in Retrofit 2.0
            stvServiceClient = RetrofitClient.getStvServiceClient(getContext());

            Call<List<NewsArticle>> callNewsArticles = stvServiceClient.getNewsArticles();
            //asynchronous call
            callNewsArticles.enqueue(this);
        }

// Onclick listerner for Listview that loads an intent to launch DetailNewsActivity and passes
        // in all the variables needed there
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Intent articleDetailIntent = new Intent(getActivity(), DetailNewsActivity.class);
                articleDetailIntent.putExtra(ARG_MAIN_TO_DETAIL_TITLE, getNewsArticleList().get(position).getTitle());
                articleDetailIntent.putExtra(ARG_MAIN_TO_DETAIL, getNewsArticleList().get(position).getContentHTML());
                articleDetailIntent.putExtra(ARG_MAIN_TO_DETAIL_IMAGE_THUMB, (Serializable) getNewsArticleList().get(position).getThumbImages());
                articleDetailIntent.putExtra(ARG_MAIN_TO_DETAIL_IMAGE, (Serializable) getNewsArticleList().get(position).getImages());
                startActivity(articleDetailIntent);

            }
        });


        return rootView;
    }

    /**
     * Retrofit response for the NewsArticles
     * @param response
     */
    @Override
    public void onResponse(Response<List<NewsArticle>> response) {
        // Check if success eg. 200, 201
        if (response.isSuccess()) {
            if (response.body() != null) {
                fetchImageForArticles(response.body());
            } else {
                // Say if returned null
                displaySnackBarOnFailure();

            }
        } else {
            // Not successful download
            displaySnackBarOnFailure();

        }
    }

    /**
     * Retrofit response failure for NewsArticles
     * @param t
     */
    @Override
    public void onFailure(Throwable t) {

        displaySnackBarOnFailure();

    }

    /**
     * Utility method for whenever information is unsuccessfully downloaded that displays a snackbar
     */
    private void displaySnackBarOnFailure() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Snackbar snackbar = Snackbar.make(mRelativeLayout, getString(R.string.error_failed_download), Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        });
    }

    /**
     * Loops through articles to download the images for them so they can be displayed in the view
     * @param articles
     */
    private void fetchImageForArticles(List<NewsArticle> articles) {

        String thumbWidth = "640";
        String thumbHeight = "360";

        // Iterate through articles
        for (final NewsArticle article : articles) {
            mArticleImage = new ArticleImage();

            //Iterate through images for each article
            for (Image image : article.getImages()) {

                // Retrofit call to get images
                Call<ArticleImage> articleImageCall = stvServiceClient.getImageWithWidthAndHeight(image.getId(), thumbWidth, thumbHeight);

                articleImageCall.enqueue(new Callback<ArticleImage>() {

                    @Override
                    public void onResponse(Response<ArticleImage> response) {
                        if (response.isSuccess()) {
                            // request successful (status code 200, 201)
                            mArticleImage = response.body();
                            if (mArticleImage != null) {
                                article.getThumbImages().add(mArticleImage);
                            }
                        } else {
                            displaySnackBarOnFailure();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        displaySnackBarOnFailure();
                    }
                });


            }

        }

// Set the global newslist
        setNewsArticleList(articles);

// Hide loader and set adapter
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                spinner.setVisibility(View.GONE);

                adapter = new NewsArticleAdapter(getActivity(), getNewsArticleList());
                mListView.setAdapter(adapter);
                ((BaseAdapter) mListView.getAdapter()).notifyDataSetChanged();
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume: ");
        mListView.setAdapter(mListView.getAdapter());
    }

    /**
     * save state for rotations and garbage collection
     * @param outState
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("newsArticle", (Serializable) getNewsArticleList());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            setNewsArticleList((List<NewsArticle>) savedInstanceState.getSerializable("newsArticle"));
        }
    }

    public List<NewsArticle> getNewsArticleList() {
        return mNewsArticleList;
    }

    public void setNewsArticleList(List<NewsArticle> newsArticleList) {
        mNewsArticleList = newsArticleList;
    }
}
